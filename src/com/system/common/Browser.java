package com.system.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser {
	
	static String chrome_binary = "";
	static String firefox_binary="";
	
	public static WebDriver chrome() {
		chrome_binary = System.getProperty("user.dir")+(System.getenv("os").contains("Windows")?
					"/libs/chromedriver.exe":"/libs/chromedriver");
		System.setProperty("webdriver.chrome.driver", chrome_binary);
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	
	public static WebDriver firefox() {

		//code goes here
		WebDriver driver = null;
		return driver;
	}
	
	public static WebDriver safari() {

		//code goes here
		WebDriver driver = null;
		return driver;
	}
	
	public static WebDriver BrowserStack(String platform, String os, String browser, String version) {
	
		//code goes here
		WebDriver driver = null;
		return driver;
	}
	 

}
