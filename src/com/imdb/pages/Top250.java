package com.imdb.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


public class Top250 {
	
	WebDriver driver;
	
	@FindBy(css=".lister-list>tr>.titleColumn>a")
	@CacheLookup
	List<WebElement> moviesName;
	
	@FindBy(css=".lister-list>tr>.ratingColumn>strong")
	@CacheLookup
	List<WebElement> moviesRating;
	
	@FindBy(className="lister-sort-by")
	WebElement sortingDrpDwn;
	
	@FindBy(css=".quicklinks>li>a")
	List<WebElement> genreList;
	
	public Top250 (WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver, this);
	}
	
	
	public boolean changeGenre(String genreName) {
		
		for (WebElement e:genreList){
			if(e.getText().equalsIgnoreCase(genreName)) {
				e.click();
				return true;
				}
		}
		return false;
		
	}
	
	public int getMovieCount(){
		
		return moviesName.size();
		
	}
	
	public int getMovieRank(String movieName) {
		
		try {
			return moviesName.indexOf(movieName);
		} catch(Exception e) {return -999;}
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
	public String getCurrentURL() {
		return driver.getCurrentUrl();
	}
	
	public boolean changeSorting(String sortItem) {
		
		Select sorting = new Select(sortingDrpDwn);
		
		try{
			if(sorting.getFirstSelectedOption().getText().equals(sortItem)) return true;
			sorting.selectByVisibleText(sortItem);
			driver.wait(5000);
			return true;
		} catch(Exception e) {return false;}
		
	}

}
