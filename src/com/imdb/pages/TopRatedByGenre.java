package com.imdb.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class TopRatedByGenre {
	
	WebDriver driver;
	
	@FindBy(css=".lister-item-header>a")
	List<WebElement> moviesName;
	
	@FindBy(css=".sorting>a")
	List<WebElement> sortingoptions;
	
	public TopRatedByGenre (WebDriver driver) {
		this.driver= driver;
		PageFactory.initElements(driver, this);
	}

	public int getMovieCount(){
		return moviesName.size();
	}
	
	public boolean changeSorting(String sortItem) {
		
		for(WebElement e : sortingoptions) {
			if(e.getText()==sortItem){
				e.click();
				return true;
			}
		}
		
		return false;		
	}
}
