package com.imdb.tests;

import org.testng.annotations.Test;

import com.imdb.pages.Top250;
import com.imdb.pages.TopRatedByGenre;
import com.system.common.Browser;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;

public class imdbTestCases {

	WebDriver driver;
	Top250 top250;
	TopRatedByGenre TopGenre;

	@BeforeTest
	public void beforeTest() {

		driver = Browser.chrome();
		driver.navigate().to("http://www.imdb.com/chart/top");
	}

	@Test
	public void TC1_CorrectPage() {

		top250 = new Top250(driver);	
		String title = top250.getTitle();
		assertEquals(title, "IMDb Top 250 - IMDb" );  
	}

	@Test(dataProvider = "SortingOptions")
	public void TC2_CheckCount(String sortItem) {
		top250.changeSorting(sortItem);
		int count = top250.getMovieCount();
		assertEquals(count>=1, true);
	}
	
	@Test(dataProvider = "GenreOptions")
	public void TC3_CheckCountinGenre(String genreName) {
		top250.changeGenre(genreName);
		TopGenre = new TopRatedByGenre(driver);
		int count = TopGenre.getMovieCount();
		assertEquals(count>=1, true);
		driver.navigate().back();
	}
	
	@AfterTest
	public void afterTest() {

		driver.quit();
	}
	
	@DataProvider(name="SortingOptions")
	public static Object[][] sortingOpts() {
		return new Object[][] {{"Ranking"}, {"IMDb Rating"},{"Release Date"},{"Number of Ratings"},{"Your Rating"}};
	}
	
	@DataProvider(name="GenreOptions")
	public static Object[][] genreOpts() {
		return new Object[][] {{"Western"}
	//						, {"Action"}
	//						,{"Adventure"}
	//						,{"Drama"}
	//						,{"Comedy"}
							};
	}

}
