# README #

This utility performs few automation tests in IMDb website. 

### How To Run ###

`C:\imdb-project>java -cp bin;libs/* org.testng.TestNG testng.xml`

### Dependencies ###

* Selenium 2.53
* TestNG 6.9.9

### Browser Support ###

Current support for *Chrome* only has been added for this utility. 

### Contribution guidelines ###

* People are free take brunch and do changes in local.
* No commit accepted. This repo is made for demo purpose only.